// WL_AUTOPLAY.CPP
// Author: LinuxWolf - Team RayCAST

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "wl_autoplay.h"
#include "wl_def.h"

#define MAX_NODES (64 * 64)
#define UPDATE_TICS 0

typedef struct Tile_s
{
	bool visited;
	int visitedDirs;
} Tile_t;

typedef struct Node_s
{
	int x, y;
	int dir;
} Node_t;

typedef struct Autoplay_s
{
	int x, y;
	int dir;
	Tile_t tiles[64][64];
	Node_t nodes[MAX_NODES];
	int numNodes;
	bool enabled;
	int32_t ticcount;
} Autoplay_t;

static Autoplay_t LWMP_DECL(autoplay);

void Autoplay_Init(int x, int y, int dir)
{
	memset(&autoplay_mp, 0, sizeof(autoplay_mp));
	autoplay_mp.x = x;
	autoplay_mp.y = y;
	autoplay_mp.dir = dir;
}

static bool Autoplay_CanPass(int x, int y)
{
	int lock;
	bool canPass = false;

	if (!TileIsSolid(x, y))
	{
		if (TileIsDoor(x, y))
		{
			lock = DoorLock(x, y);
			if (lock == dr_normal || lock == dr_elevator)
			{
				canPass = true;
			}
			else if (lock == dr_lock1)
			{
				if (gamestate.keys_mp & 1)
				{
					canPass = true;
				}
			}
			else if (lock == dr_lock2)
			{
				if (gamestate.keys_mp & 2)
				{
					canPass = true;
				}
			}
		}
		else
		{
			canPass = true;
		}
	}

	return canPass;
}

void Autoplay_Update(void)
{
	int i;
	int x, y;
	int dir;
	int x2, y2;
	Tile_t *tile;
	Node_t node;
	int moves[4][2] =
	{
		{ 1, 0 }, 
		{ 0, 1 },
		{ -1, 0 },
		{ 0, -1 },
	};
	int invDir[4] = { 2, 3, 0, 1 };
	int randDir;

	autoplay_mp.ticcount = 0;

	x = autoplay_mp.x;
	y = autoplay_mp.y;

	tile = &autoplay_mp.tiles[x][y];

	assert(/*!TileIsSolid(x, y) && */
		x >= 0 && x < 64 && y >= 0 && y < 64);

	tile->visited = true;

	randDir = US_RndT() % 4;

	for (i = 0; i < 4; i++)
	{
		dir = (randDir + i) % 4;
		x2 = x + moves[dir][0];
		y2 = y + moves[dir][1];

		if (Autoplay_CanPass(x2, y2) && 
			!(tile->visitedDirs & (1 << dir)) &&
			!autoplay_mp.tiles[x2][y2].visited)
		{
			break;
		}
	}

	if (i != 4)
	{
		tile->visitedDirs |= (1 << dir);

		node.x = x;
		node.y = y;
		node.dir = dir;
		if (autoplay_mp.numNodes < MAX_NODES)
		{
			autoplay_mp.nodes[autoplay_mp.numNodes++] = node;
		}

		autoplay_mp.x = x2;
		autoplay_mp.y = y2;
		autoplay_mp.dir = dir;
	}
	else
	{
		// backtrack if possible
		if (autoplay_mp.numNodes > 0)
		{
			node = autoplay_mp.nodes[--autoplay_mp.numNodes];

			autoplay_mp.x = node.x;
			autoplay_mp.y = node.y;
			autoplay_mp.dir = invDir[node.dir];
		}
	}
}

Autoplay_Pos_t Autoplay_GetPos(void)
{
	Autoplay_Pos_t pos;
	pos.x = autoplay_mp.x;
	pos.y = autoplay_mp.y;
	pos.dir = autoplay_mp.dir;
	return pos;
}

bool Autoplay_UpdateRequired(void)
{
	autoplay_mp.ticcount += tics;
	return autoplay_mp.enabled && 
		(autoplay_mp.ticcount >= UPDATE_TICS ||
		Keyboard[sc_G]);
}

void Autoplay_ToggleEnabled(void)
{
	autoplay_mp.enabled = !autoplay_mp.enabled;
	if (autoplay_mp.enabled)
	{
		Autoplay_Init(player_mp->tilex, 63 - player_mp->tiley, 0);
		autoplay_mp.enabled = true;
	}
}

void Autoplay_GotKey(void)
{
	int c;

	for (LWMP_REPEAT(c))
	{
		if (autoplay_mp.enabled)
		{
			Autoplay_Init(player_mp->tilex, 
				63 - player_mp->tiley, 0);
			autoplay_mp.enabled = true;
		}
	}
}
