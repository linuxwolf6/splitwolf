#ifndef WL_TYPES_H
#define WL_TYPES_H

#include <assert.h>
#include <fcntl.h>
#include <math.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#if defined(_arch_dreamcast)
#include <string.h>
#elif !defined(_WIN32)
#include <string.h>
#include <stdarg.h>
#endif
#include <stdint.h>

typedef uint8_t byte;
typedef uint16_t word;
typedef int32_t fixed;
typedef uint32_t longword;
typedef int8_t boolean;
typedef void * memptr;

#endif
