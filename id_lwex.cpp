// ID_LWEX.CPP // Created Sun Feb 26 17:54:03 2012
// Author: LinuxWolf - Team RayCAST

#include <stdio.h>
#include <assert.h>
#include "id_lwex.h"
#include "wl_def.h"
#include "fixedptc.h"
#include "uthash.h"

typedef struct LWEX_Hash_User_s
{
    int key;
    int val;
    UT_hash_handle hh;
} LWEX_Hash_User_t;

typedef struct LWEX_Hash_Priv_s
{
    LWEX_Hash_User_t *users;
} LWEX_Hash_Priv_t;

int LWEX_GetTile(vec2i_t pos)
{
    int tile = 0;
    if (X(pos) >= 0 && X(pos) < 64 && Y(pos) >= 0 && Y(pos) < 64)
    {
        tile = (int)tilemap[X(pos)][64 - Y(pos) - 1];
    }
    return tile;
}

byte LWEX_GetMinimapRevealTile(vec2i_t pos)
{
    byte reveal = 0;
    if (X(pos) >= 0 && X(pos) < 64 && Y(pos) >= 0 && Y(pos) < 64)
    {
        reveal = gamestate.minimap_reveal[X(pos)][63 - Y(pos)];
    }
    return reveal;
}

void LWEX_SetMinimapRevealTile(vec2i_t pos, byte reveal)
{
    if (X(pos) >= 0 && X(pos) < 64 && 
        Y(pos) >= 0 && Y(pos) < 64)
    {
        gamestate.minimap_reveal[X(pos)][63 - Y(pos)] = 
            reveal;
    }
}

doorobj_t *LWEX_GetDoor(vec2i_t pos)
{
    int tile;
    int doorIndex;
    
    tile = LWEX_GetTile(pos);
    doorIndex = tile & 0x7f;

    assert(doorIndex >= 0 && doorIndex < doornum);
    return &doorobjlist[doorIndex];
}

bool LWEX_TileIsWall(vec2i_t pos)
{
    int tile = LWEX_GetTile(pos);
    return tile != 0 && tile < AREATILE;
}

bool LWEX_TileIsDoor(vec2i_t pos)
{
    return (LWEX_GetTile(pos) & 0x80) != 0;
}

bool LWEX_NextTileIsWall(vec2i_t pos, dir4_t dir)
{
    return LWEX_TileIsWall(vec2i_neigh(pos, dir));
}

bool LWEX_TileIsPushWall(vec2i_t pos)
{
    return pwallstate != 0 && X(pos) == pwallx && Y(pos) == 
        63 - pwally;
}

bool LWEX_TileIsPushable(vec2i_t pos)
{
    return LWEX_TileIsWall(pos) && 
        *(mapsegs[1] + ((63 - Y(pos)) << mapshift) + X(pos)) == 
        PUSHABLETILE;
}

bool LWEX_DoorIsOpen(vec2i_t pos)
{
    return LWEX_GetDoor(pos)->action != dr_closed;
}

bool LWEX_DoorIsVertical(vec2i_t pos)
{
    return LWEX_GetDoor(pos)->vertical;
}

bool LWEX_TileIsElevator(vec2i_t pos)
{
    int tile = LWEX_GetTile(pos);
    return tile == ELEVATORTILE;
}

vec3f_t vec3f(float x, float y, float z)
{
    vec3f_t a;
    C(a, 0) = x;
    C(a, 1) = y;
    C(a, 2) = z;
    return a;
}

mat3f_t mat3f_ident(void)
{
    mat3f_t m = 
    {
        { 
            1.0, 0.0, 0.0,
            0.0, 1.0, 0.0,
            0.0, 0.0, 1.0,
        },
    };
    return m;
}

mat3f_t mat3f_mult(mat3f_t a, mat3f_t b)
{
    int i, j, k;
    mat3f_t c;

    memset(&c, 0, sizeof(c));
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 3; k++)
            {
                CM(c, i, j) += CM(a, i, k) * CM(b, k, j);
            }
        }
    }

    return c;
}

vec3f_t mat3f_mult_point(mat3f_t a, vec3f_t b)
{
    return vec3f(
        CM(a, 0, 0) * C(b, 0) + CM(a, 0, 1) * C(b, 1) + CM(a, 0, 2) * C(b, 2),
        CM(a, 1, 0) * C(b, 0) + CM(a, 1, 1) * C(b, 1) + CM(a, 1, 2) * C(b, 2),
        CM(a, 2, 0) * C(b, 0) + CM(a, 2, 1) * C(b, 1) + CM(a, 2, 2) * C(b, 2)
        );
}

vec3fixed_t vec3fixed_anglevec(int32_t angle)
{
    fixed cosv, sinv;

    while (angle < 0)
    {
        angle += ANGLES;
    }
    while (angle >= ANGLES)
    {
        angle -= ANGLES;
    }

    cosv = costable[angle];
    sinv = sintable[angle];

    return vec3fixed(cosv, sinv, FP(0));
}

mat3fixed_t mat3fixed_zero(void)
{
    mat3fixed_t m = 
    {
        { 
            FP(0), FP(0), FP(0),
            FP(0), FP(0), FP(0),
            FP(0), FP(0), FP(0),
        },
    };
    return m;
}

mat3fixed_t mat3fixed_ident(void)
{
    mat3fixed_t m = 
    {
        { 
            FP(1), FP(0), FP(0),
            FP(0), FP(1), FP(0),
            FP(0), FP(0), FP(1),
        },
    };
    return m;
}

mat3fixed_t mat3fixed_diag(fixed a, fixed b, fixed c)
{
    mat3fixed_t m = mat3fixed_zero();
    CM(m, 0, 0) = a;
    CM(m, 1, 1) = b;
    CM(m, 2, 2) = c;
    return m;
}

vec3fixed_t mat3fixed_mult_point(mat3fixed_t a, vec3fixed_t b)
{
    return vec3fixed(
        FixedMul(CM(a, 0, 0), C(b, 0)) + 
        FixedMul(CM(a, 0, 1), C(b, 1)) +
        FixedMul(CM(a, 0, 2), C(b, 2)),
        FixedMul(CM(a, 1, 0), C(b, 0)) +
        FixedMul(CM(a, 1, 1), C(b, 1)) +
        FixedMul(CM(a, 1, 2), C(b, 2)),
        FixedMul(CM(a, 2, 0), C(b, 0)) +
        FixedMul(CM(a, 2, 1), C(b, 1)) +
        FixedMul(CM(a, 2, 2), C(b, 2))
        );
}

mat3fixed_t mat3fixed_rot(int32_t angle)
{
    mat3fixed_t rotmat;
    fixed cosv, sinv;

    while (angle < 0)
    {
        angle += ANGLES;
    }
    while (angle >= ANGLES)
    {
        angle -= ANGLES;
    }

    cosv = costable[angle];
    sinv = sintable[angle];

    rotmat = mat3fixed_ident();
    C(rotmat, 0) = cosv;
    C(rotmat, 1) = -sinv;
    C(rotmat, 3) = sinv;
    C(rotmat, 4) = cosv;

    return rotmat;
}

mat3fixed_t mat3fixed_transpose(mat3fixed_t a)
{
    int i, j;
    mat3fixed_t b;

    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
        {
            CM(b, i, j) = CM(a, j, i);
        }
    }

    return b;
}

int LWEX_Log2(int x)
{
    int res = 0;
    while (x > 1)
    {
        res++;
        x >>= 1;
    }
    return res;
}

LWEX_Hash_t LWEX_Hash_New(void)
{
    LWEX_Hash_t hash;
    LWEX_Hash_Priv_t *priv;

    hash.priv = malloc(sizeof(LWEX_Hash_Priv_t));
    CHECKMALLOCRESULT(hash.priv);

    priv = (LWEX_Hash_Priv_t *)hash.priv;
    priv->users = NULL;

    return hash;
}

void LWEX_Hash_Add(LWEX_Hash_t hash, int key, int val)
{
    LWEX_Hash_Priv_t *priv;
    LWEX_Hash_User_t *user;

    priv = (LWEX_Hash_Priv_t *)hash.priv;

    user = (LWEX_Hash_User_t *)malloc(
        sizeof(LWEX_Hash_User_t));
    user->key = key;
    user->val = val;
    HASH_ADD_INT(priv->users, key, user);
}

int LWEX_Hash_Find(LWEX_Hash_t hash, int key)
{
    LWEX_Hash_Priv_t *priv;
    LWEX_Hash_User_t *user;

    priv = (LWEX_Hash_Priv_t *)hash.priv;

    user = NULL;
    HASH_FIND_INT(priv->users, &key, user);

    return (user != NULL) ? user->val : -1;
}
