//
//	ID Engine
//	ID_IN.c - Input Manager
//	v1.0d1
//	By Jason Blochowiak
//

//
//	This module handles dealing with the various input devices
//
//	Depends on: Memory Mgr (for demo recording), Sound Mgr (for timing stuff),
//				User Mgr (for command line parms)
//
//	Globals:
//		LastScan - The keyboard scan code of the last key pressed
//		LastASCII - The ASCII value of the last key pressed
//	DEBUG - there are more globals
//

#include "wl_def.h"

/*
=============================================================================

					GLOBAL VARIABLES

=============================================================================
*/


//
// configuration variables
//
boolean MousePresent;
boolean forcegrabmouse;


// 	Global variables
/*volatile*/ std::map<ScanCode, boolean>    Keyboard;
volatile boolean	Paused;
volatile char		  LastASCII;
         ScanCode	LastScan;
volatile boolean  MouseWheel[2];

//KeyboardDef	KbdDefs = {0x1d,0x38,0x47,0x48,0x49,0x4b,0x4d,0x4f,0x50,0x51};
static KeyboardDef KbdDefs = {
    sc_Ctrl,                // button0
    sc_Alt,                 // button1
    sc_Home,                // upleft
    sc_UpArrow,             // up
    sc_PgUp,                // upright
    sc_LeftArrow,           // left
    sc_RightArrow,          // right
    sc_End,                 // downleft
    sc_DownArrow,           // down
    sc_PgDn                 // downright
};



SDL_GameController *GameControllers[4];
SDL_Joystick *Joysticks[4];

static bool GrabInput = false;

/*
=============================================================================

					LOCAL VARIABLES

=============================================================================
*/
byte        ASCIINames[] =		// Unshifted ASCII for scan codes       // TODO: keypad
{
//	 0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
	0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,8  ,9  ,0  ,0  ,0  ,13 ,0  ,0  ,	// 0
    0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,27 ,0  ,0  ,0  ,	// 1
	' ',0  ,0  ,0  ,0  ,0  ,0  ,39 ,0  ,0  ,'*','+',',','-','.','/',	// 2
	'0','1','2','3','4','5','6','7','8','9',0  ,';',0  ,'=',0  ,0  ,	// 3
	'`','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o',	// 4
	'p','q','r','s','t','u','v','w','x','y','z','[',92 ,']',0  ,0  ,	// 5
	0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,	// 6
	0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0		// 7
};
byte ShiftNames[] =		// Shifted ASCII for scan codes
{
//	 0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
	0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,8  ,9  ,0  ,0  ,0  ,13 ,0  ,0  ,	// 0
    0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,27 ,0  ,0  ,0  ,	// 1
	' ',0  ,0  ,0  ,0  ,0  ,0  ,34 ,0  ,0  ,'*','+','<','_','>','?',	// 2
	')','!','@','#','$','%','^','&','*','(',0  ,':',0  ,'+',0  ,0  ,	// 3
	'~','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O',	// 4
	'P','Q','R','S','T','U','V','W','X','Y','Z','{','|','}',0  ,0  ,	// 5
	0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,	// 6
	0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0		// 7
};
byte SpecialNames[] =	// ASCII for 0xe0 prefixed codes
{
//	 0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
	0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,	// 0
	0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,13 ,0  ,0  ,0  ,	// 1
	0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,	// 2
	0  ,0  ,0  ,0  ,0  ,'/',0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,	// 3
	0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,	// 4
	0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,	// 5
	0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,	// 6
	0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0   	// 7
};


static	boolean		IN_Started;

static	Direction	DirTable[] =		// Quick lookup for total direction
{
    dir_NorthWest,	dir_North,	dir_NorthEast,
    dir_West,		dir_None,	dir_East,
    dir_SouthWest,	dir_South,	dir_SouthEast
};


///////////////////////////////////////////////////////////////////////////
//
//	INL_GetMouseButtons() - Gets the status of the mouse buttons from the
//		mouse driver
//
///////////////////////////////////////////////////////////////////////////
static int
INL_GetMouseButtons(void)
{
    int buttons = SDL_GetMouseState(NULL, NULL);
    int middlePressed = buttons & SDL_BUTTON(SDL_BUTTON_MIDDLE);
    int rightPressed = buttons & SDL_BUTTON(SDL_BUTTON_RIGHT);
    buttons &= ~(SDL_BUTTON(SDL_BUTTON_MIDDLE) | SDL_BUTTON(SDL_BUTTON_RIGHT));
    if(middlePressed) buttons |= 1 << 2;
    if(rightPressed) buttons |= 1 << 1;

    buttons &= ~(1 << 3);
    if (MouseWheel[0])
    {
        buttons |= 1 << 3;
        MouseWheel[0] = false;
    }

    buttons &= ~(1 << 4);
    if (MouseWheel[1])
    {
        buttons |= 1 << 4;
        MouseWheel[1] = false;
    }

    return buttons;
}

///////////////////////////////////////////////////////////////////////////
//
//	IN_GetJoyDelta() - Returns the relative movement of the specified
//		joystick (from +/-127)
//
///////////////////////////////////////////////////////////////////////////
void IN_GetJoyDelta(int *dx,int *dy,int joyIndex,SDL_GameControllerAxis xaxis,SDL_GameControllerAxis yaxis, bool useDpad)
{
    int x;
    int y;

    SDL_GameController *GameController = GameControllers[joyIndex];
    SDL_Joystick *Joystick = Joysticks[joyIndex];

    if(GameController) {
        // TODO: Should we avoid calling this for each player every tick?
        SDL_GameControllerUpdate();

        x = SDL_GameControllerGetAxis(GameController, xaxis) >> 8;
        y = SDL_GameControllerGetAxis(GameController, yaxis) >> 8;

        if (useDpad)
        {
            if(SDL_GameControllerGetButton(GameController, SDL_CONTROLLER_BUTTON_DPAD_RIGHT))
                x += 127;
            else if(SDL_GameControllerGetButton(GameController, SDL_CONTROLLER_BUTTON_DPAD_LEFT))
                x -= 127;
            if(SDL_GameControllerGetButton(GameController, SDL_CONTROLLER_BUTTON_DPAD_DOWN))
                y += 127;
            else if(SDL_GameControllerGetButton(GameController, SDL_CONTROLLER_BUTTON_DPAD_UP))
                y -= 127;

            if(x < -128) x = -128;
            else if(x > 127) x = 127;

            if(y < -128) y = -128;
            else if(y > 127) y = 127;
        }
    }
    else if (Joystick) {
        // TODO: Should we avoid calling this for each player every tick?
        SDL_JoystickUpdate();

        x = SDL_JoystickGetAxis(Joystick, xaxis) >> 8;
        y = SDL_JoystickGetAxis(Joystick, yaxis) >> 8;

        if (useDpad)
        {
            int hat = SDL_JoystickGetHat(Joystick, 0);
            if(hat == SDL_HAT_RIGHT || hat == SDL_HAT_RIGHTUP || hat == SDL_HAT_RIGHTDOWN)
                x += 127;
            else if(hat == SDL_HAT_LEFT || hat == SDL_HAT_LEFTUP || hat == SDL_HAT_LEFTDOWN)
                x -= 127;
            if(hat == SDL_HAT_DOWN || hat == SDL_HAT_RIGHTUP || hat == SDL_HAT_LEFTUP)
                y += 127;
            else if(hat == SDL_HAT_UP || hat == SDL_HAT_RIGHTDOWN || hat == SDL_HAT_LEFTDOWN)
                y -= 127;

            if(x < -128) x = -128;
            else if(x > 127) x = 127;

            if(y < -128) y = -128;
            else if(y > 127) y = 127;
        }
    }
    else
    {
        *dx = *dy = 0;
        return;
    }

    // Apply expo
    x = (int) (pow(x / 127.0, JOYEXPO) * (x > 0 ? 127 : -127));
    y = (int) (pow(y / 127.0, JOYEXPO) * (y > 0 ? 127 : -127));

    *dx = x;
    *dy = y;
}

/*
===================
=
= IN_JoyButtons
=
===================
*/

int IN_JoyButtons(int joyIndex)
{
    int res = 0;
    SDL_GameController *GameController = GameControllers[joyIndex];
    SDL_Joystick *Joystick = Joysticks[joyIndex];

    if(GameController) {
        // TODO: Should we avoid calling this for each player every tick?
        SDL_GameControllerUpdate();

        // Read in each button in the order we expect to define it
        res |= SDL_GameControllerGetButton(GameController, SDL_CONTROLLER_BUTTON_A) << 0;
        res |= SDL_GameControllerGetButton(GameController, SDL_CONTROLLER_BUTTON_B) << 1;
        res |= SDL_GameControllerGetButton(GameController, SDL_CONTROLLER_BUTTON_X) << 2;
        res |= SDL_GameControllerGetButton(GameController, SDL_CONTROLLER_BUTTON_Y) << 3;
        res |= SDL_GameControllerGetButton(GameController, SDL_CONTROLLER_BUTTON_LEFTSHOULDER) << 4;
        res |= SDL_GameControllerGetButton(GameController, SDL_CONTROLLER_BUTTON_RIGHTSHOULDER) << 5;
        // Read triggers in as buttons
        res |= ((SDL_GameControllerGetAxis(GameController, SDL_CONTROLLER_AXIS_TRIGGERLEFT) >> 8) > TRIGGERDEADZONE) << 6;
        res |= ((SDL_GameControllerGetAxis(GameController, SDL_CONTROLLER_AXIS_TRIGGERRIGHT) >> 8) > TRIGGERDEADZONE) << 7;
        res |= SDL_GameControllerGetButton(GameController, SDL_CONTROLLER_BUTTON_LEFTSTICK) << 8;
        res |= SDL_GameControllerGetButton(GameController, SDL_CONTROLLER_BUTTON_RIGHTSTICK) << 9;
        res |= SDL_GameControllerGetButton(GameController, SDL_CONTROLLER_BUTTON_BACK) << 10;
        res |= SDL_GameControllerGetButton(GameController, SDL_CONTROLLER_BUTTON_START) << 11;
        res |= SDL_GameControllerGetButton(GameController, SDL_CONTROLLER_BUTTON_GUIDE) << 12;
    }
    else if (Joystick) {
        // TODO: Should we avoid calling this for each player every tick?
        SDL_JoystickUpdate();

        // Read in each button
        res |= SDL_JoystickGetButton(Joystick, 0) << 0; // A
        res |= SDL_JoystickGetButton(Joystick, 1) << 1; // B
        res |= SDL_JoystickGetButton(Joystick, 2) << 2; // X
        res |= SDL_JoystickGetButton(Joystick, 3) << 3; // Y
        res |= SDL_JoystickGetButton(Joystick, 4) << 4; // L1
        res |= SDL_JoystickGetButton(Joystick, 5) << 5; // R1
        res |= SDL_JoystickGetButton(Joystick, 6) << 6; // L2
        res |= SDL_JoystickGetButton(Joystick, 7) << 7; // R2
        res |= SDL_JoystickGetButton(Joystick, 11) << 8; // LS
        res |= SDL_JoystickGetButton(Joystick, 12) << 9; // RS
        res |= SDL_JoystickGetButton(Joystick, 8) << 10; // Select
        res |= SDL_JoystickGetButton(Joystick, 9) << 11; // Start
        res |= SDL_JoystickGetButton(Joystick, 10) << 12; // Guide
    }

    return res;
}


boolean IN_JoyPresent(void)
{
    return GameControllers[0] != NULL || Joysticks[0] != NULL;
}

boolean IN_JoyPresent(int joyIndex)
{
    return (joyIndex <= 4) && (GameControllers[joyIndex] != NULL || Joysticks[joyIndex] != NULL);
}

static void processEvent(SDL_Event *event)
{
    switch (event->type)
    {
        // exit if the window is closed
        case SDL_QUIT:
            Quit(NULL);

        // check for keypresses
        case SDL_KEYDOWN:
        {
            if(event->key.keysym.sym==SDLK_SCROLLLOCK || event->key.keysym.sym==SDLK_F12)
            {
                GrabInput = !GrabInput;
                SDL_SetWindowGrab(window, GrabInput ? SDL_TRUE : SDL_FALSE);
                return;
            }

            LastScan = event->key.keysym.sym;
            SDL_Keymod mod = SDL_GetModState();
            if(Keyboard[sc_Alt])
            {
                if(LastScan==SDLK_F4)
                    Quit(NULL);
            }

            if(LastScan == SDLK_KP_ENTER) LastScan = SDLK_RETURN;
            //else if(LastScan == SDLK_RSHIFT) LastScan = SDLK_LSHIFT;
            //else if(LastScan == SDLK_RALT) LastScan = SDLK_LALT;
            //else if(LastScan == SDLK_RCTRL) LastScan = SDLK_LCTRL;
            else
            {
                if((mod & KMOD_NUM) == 0)
                {
                    switch(LastScan)
                    {
                        case SDLK_KP_2: LastScan = SDLK_DOWN; break;
                        case SDLK_KP_4: LastScan = SDLK_LEFT; break;
                        case SDLK_KP_6: LastScan = SDLK_RIGHT; break;
                        case SDLK_KP_8: LastScan = SDLK_UP; break;
                    }
                }
            }

            int sym = LastScan;
            if(sym >= 'a' && sym <= 'z')
                sym -= 32;  // convert to uppercase

            if(mod & (KMOD_SHIFT | KMOD_CAPS))
            {
                if(sym < lengthof(ShiftNames) && ShiftNames[sym])
                    LastASCII = ShiftNames[sym];
            }
            else
            {
                if(sym < lengthof(ASCIINames) && ASCIINames[sym])
                    LastASCII = ASCIINames[sym];
            }
            // if(LastScan<SDLK_LAST)
            Keyboard[LastScan] = 1;
            if(LastScan == SDLK_PAUSE)
                Paused = true;
            break;
		}

        case SDL_KEYUP:
        {
            int key = event->key.keysym.sym;
            if(key == SDLK_KP_ENTER) key = SDLK_RETURN;
            //else if(key == SDLK_RSHIFT) key = SDLK_LSHIFT;
            //else if(key == SDLK_RALT) key = SDLK_LALT;
            //else if(key == SDLK_RCTRL) key = SDLK_LCTRL;
            else
            {
                if((SDL_GetModState() & KMOD_NUM) == 0)
                {
                    switch(key)
                    {
                        case SDLK_KP_2: key = SDLK_DOWN; break;
                        case SDLK_KP_4: key = SDLK_LEFT; break;
                        case SDLK_KP_6: key = SDLK_RIGHT; break;
                        case SDLK_KP_8: key = SDLK_UP; break;
                    }
                }
            }

            // if(key<SDLK_LAST)
            Keyboard[key] = 0;
            break;
        }

#if defined(GP2X)
        case SDL_JOYBUTTONDOWN:
            GP2X_ButtonDown(event->jbutton.button);
            break;

        case SDL_JOYBUTTONUP:
            GP2X_ButtonUp(event->jbutton.button);
            break;
#endif
        /*
        TODO: enable mousewheel support
        case SDL_MOUSEBUTTONDOWN:
            switch (event->button.button)
            {
            case SDL_BUTTON_WHEELUP:
                MouseWheel[0] = true;
                break;
            case SDL_BUTTON_WHEELDOWN:
                MouseWheel[1] = true;
                break;
            }
            break;
        */
    }
}

void IN_WaitAndProcessEvents()
{
    SDL_Event event;
    if(!SDL_WaitEvent(&event)) return;
    do
    {
        processEvent(&event);
    }
    while(SDL_PollEvent(&event));
}

void IN_ProcessEvents()
{
    SDL_Event event;

    while (SDL_PollEvent(&event))
    {
        processEvent(&event);
    }
}

///////////////////////////////////////////////////////////////////////////
//
//	IN_Startup() - Starts up the Input Mgr
//
///////////////////////////////////////////////////////////////////////////
void
IN_Startup(void)
{
    if (IN_Started)
      return;

    IN_ClearKeysDown();

    // Add controller mappings
    SDL_GameControllerAddMappingsFromFile("gamecontrollerdb.txt");

    SDL_GameController *GameController;
    SDL_Joystick *Joystick;
    if(param_joystickindex >= 0 && param_joystickindex < SDL_NumJoysticks())
    {
        int gameControllerCount = 0;
        for (int i = 0; i < SDL_NumJoysticks() && i < 4; i++) {
            int gameControllerIndex = param_joystickindex + i;

            SDL_JoystickGUID guid = SDL_JoystickGetDeviceGUID(gameControllerIndex);
            char *mapping = SDL_GameControllerMappingForGUID(guid);

            if (mapping) {
                Joysticks[gameControllerCount] = NULL;
                GameController = GameControllers[gameControllerCount++] = SDL_GameControllerOpen(gameControllerIndex);
                // printf("Joystick %i (%s) has GameController mapping: %s\n", gameControllerIndex, SDL_GameControllerName(GameController), mapping);
            }
            else {
                GameControllers[gameControllerCount] = NULL;
                Joystick = Joysticks[gameControllerCount++] = SDL_JoystickOpen(gameControllerIndex);
                // printf("Joystick %i (%s) is not a game controller\n", gameControllerIndex, SDL_JoystickName(Joystick));
            }
        }
    }

    SDL_EventState(SDL_MOUSEMOTION, SDL_IGNORE);

    if(fullscreen || forcegrabmouse)
    {
        GrabInput = true;
        SDL_SetWindowGrab(window, SDL_TRUE);
    }

    // I didn't find a way to ask libSDL whether a mouse is present, yet...
#if defined(GP2X)
    MousePresent = false;
#elif defined(_arch_dreamcast)
    MousePresent = DC_MousePresent();
#else
    MousePresent = true;
#endif

    IN_Started = true;
}

///////////////////////////////////////////////////////////////////////////
//
//	IN_Shutdown() - Shuts down the Input Mgr
//
///////////////////////////////////////////////////////////////////////////
void
IN_Shutdown(void)
{
	if (!IN_Started)
		return;

    for (int i = 0; i < 4; i++) {
        if (GameControllers[i]) {
            SDL_GameControllerClose(GameControllers[i]);
        }
    }

	IN_Started = false;
}

///////////////////////////////////////////////////////////////////////////
//
//	IN_ClearKeysDown() - Clears the keyboard array
//
///////////////////////////////////////////////////////////////////////////
void
IN_ClearKeysDown(void)
{
	LastScan = sc_None;
	LastASCII = key_None;
	Keyboard.clear();
}


///////////////////////////////////////////////////////////////////////////
//
//	IN_ReadControl() - Reads the device associated with the specified
//		player and fills in the control info struct
//
///////////////////////////////////////////////////////////////////////////
void
IN_ReadControl(int player_val,ControlInfo *info)
{
	word		buttons;
	int			dx,dy;
	Motion		mx,my;

	dx = dy = 0;
	mx = my = motion_None;
	buttons = 0;

	IN_ProcessEvents();

    if (Keyboard[KbdDefs.upleft])
        mx = motion_Left,my = motion_Up;
    else if (Keyboard[KbdDefs.upright])
        mx = motion_Right,my = motion_Up;
    else if (Keyboard[KbdDefs.downleft])
        mx = motion_Left,my = motion_Down;
    else if (Keyboard[KbdDefs.downright])
        mx = motion_Right,my = motion_Down;

    if (Keyboard[KbdDefs.up])
        my = motion_Up;
    else if (Keyboard[KbdDefs.down])
        my = motion_Down;

    if (Keyboard[KbdDefs.left])
        mx = motion_Left;
    else if (Keyboard[KbdDefs.right])
        mx = motion_Right;

    if (Keyboard[KbdDefs.button0])
        buttons += 1 << 0;
    if (Keyboard[KbdDefs.button1])
        buttons += 1 << 1;

	dx = mx * 127;
	dy = my * 127;

	info->x = dx;
	info->xaxis = mx;
	info->y = dy;
	info->yaxis = my;
    info->button0 = (buttons & (1 << 0)) != 0;
    info->button1 = (buttons & (1 << 1)) != 0;
    info->button2 = (buttons & (1 << 2)) != 0;
    info->button3 = (buttons & (1 << 3)) != 0;
    info->button4 = (buttons & (1 << 4)) != 0;
    info->button5 = (buttons & (1 << 5)) != 0;
    info->button6 = (buttons & (1 << 6)) != 0;
    info->button7 = (buttons & (1 << 7)) != 0;
    info->button8 = (buttons & (1 << 8)) != 0;
    info->button9 = (buttons & (1 << 9)) != 0;
    info->button10 = (buttons & (1 << 10)) != 0;
    info->button11 = (buttons & (1 << 11)) != 0;
    info->button12 = (buttons & (1 << 12)) != 0;
	info->dir = DirTable[((my + 1) * 3) + (mx + 1)];
}

///////////////////////////////////////////////////////////////////////////
//
//	IN_WaitForKey() - Waits for a scan code, then clears LastScan and
//		returns the scan code
//
///////////////////////////////////////////////////////////////////////////
ScanCode
IN_WaitForKey(void)
{
	ScanCode	result;

	while ((result = LastScan)==0)
		IN_WaitAndProcessEvents();
	LastScan = 0;
	return(result);
}

///////////////////////////////////////////////////////////////////////////
//
//	IN_WaitForASCII() - Waits for an ASCII char, then clears LastASCII and
//		returns the ASCII value
//
///////////////////////////////////////////////////////////////////////////
char
IN_WaitForASCII(void)
{
	char		result;

	while ((result = LastASCII)==0)
		IN_WaitAndProcessEvents();
	LastASCII = '\0';
	return(result);
}

///////////////////////////////////////////////////////////////////////////
//
//	IN_Ack() - waits for a button or key press.  If a button is down, upon
// calling, it must be released for it to be recognized
//
///////////////////////////////////////////////////////////////////////////

boolean	btnstate[NUMBUTTONS];

void IN_StartAck(void)
{
    IN_ProcessEvents();
//
// get initial state of everything
//
	IN_ClearKeysDown();
	memset(btnstate, 0, sizeof(btnstate));

	int buttons = IN_JoyButtons(0) << 4;

	if(MousePresent)
		buttons |= IN_MouseButtons();

	for(int i = 0; i < NUMBUTTONS; i++, buttons >>= 1)
		if(buttons & 1)
			btnstate[i] = true;
}


boolean IN_CheckAck (void)
{
    IN_ProcessEvents();
//
// see if something has been pressed
//
	if(LastScan)
		return true;

    int c;
    for (LWMP_REPEAT(c))
    {
    	int buttons = IN_JoyButtons(joyIndex_mp) << 4;

    	if(MousePresent)
    		buttons |= IN_MouseButtons();

    	for(int i = 0; i < NUMBUTTONS; i++, buttons >>= 1)
    	{
    		if(buttons & 1)
    		{
    			if(!btnstate[i])
                {
                    // Wait until button has been released
                    do
                    {
                        IN_WaitAndProcessEvents();
                        buttons = IN_JoyButtons(joyIndex_mp) << 4;

                        if(MousePresent)
                            buttons |= IN_MouseButtons();
                    }
                    while(buttons & (1 << i));

    				return true;
                }
    		}
    		else
    			btnstate[i] = false;
    	}
    }

	return false;
}


void IN_Ack (void)
{
	IN_StartAck ();

    do
    {
        IN_WaitAndProcessEvents();
    }
	while(!IN_CheckAck ());
}


///////////////////////////////////////////////////////////////////////////
//
//	IN_UserInput() - Waits for the specified delay time (in ticks) or the
//		user pressing a key or a mouse button. If the clear flag is set, it
//		then either clears the key or waits for the user to let the mouse
//		button up.
//
///////////////////////////////////////////////////////////////////////////
boolean IN_UserInput(longword delay)
{
	longword	lasttime;

	lasttime = GetTimeCount();
	IN_StartAck ();
	do
	{
        IN_ProcessEvents();
		if (IN_CheckAck())
			return true;
        SDL_Delay(5);
	} while (GetTimeCount() - lasttime < delay);
	return(false);
}

//===========================================================================

/*
===================
=
= IN_MouseButtons
=
===================
*/
int IN_MouseButtons (void)
{
	if (MousePresent)
		return INL_GetMouseButtons();
	else
		return 0;
}

bool IN_IsInputGrabbed()
{
    return GrabInput;
}

void IN_CenterMouse()
{
    SDL_WarpMouseInWindow(window, LWMP_GetPhysicalScreenWidth() / 2, LWMP_GetPhysicalScreenHeight() / 2);
}
